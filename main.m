% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ %
% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ %
%                                                                         %
%                                                                         %
%                SPACECRAFT ATTITUDE DYNAMICS & CONTROL                   %
%                                  ~                                      % 
%                      DETUMBLING and SUN-POINTING                        %
%                      of GEO-like COMM SATELLITE                         %
%                                  ~                                      %
%                           Lorenzo Ticozzi                               %
%                                                                         %
% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ %
% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ %

%%  

close all
clear
clc

%% - DETUMBLING PHASE - From 0 to 460 secs 

% - S/C data and orbital parameters. The data of the spacecraft are taken 
%   from http://www.arianespace.com/wp-content/uploads/2017/01/VS16-launchkit-EN-2.pdf
%   and describe the Hispasat communication satellite built by OHB System
%   AG. 

% - Sketch of S/C with non deployed solar panels
%
%         ___________________________
%        | \                          \        y   ^ 
%        |  \                          \           |
%        |   \                          \          |
%        |    \ d                        \         |------>
%        |     \                          \              x
%        |      \                          \
%        |       \__________________________\
%        |       |                          |      d = depth
%        |       |                          |      h = heigth
%        |       |                          |      w = width
%         \      |                          |
%          \     |                       h  |
%           \    |                          | 
%            \   |                          |
%             \  |                          |
%              \ |            w             |
%               \|__________________________|

% - Size
w = 3.1;                             % width  - [m]
h = 2.47;                            % heigth - [m]
d = 4.95;                            % depth  - [m]
sizes = [w h d]';                    
m = 4000;                            % mass   - [kg]

% - Compute the inertia assuming that during the detumbling the S/C can be
%   approximated as a cuboid
Ix_sc = 1/12*m*(h^2+d^2);       % [kg*m^2]
Iy_sc = 1/12*m*(w^2+d^2);       % [kg*m^2]
Iz_sc = 1/12*m*(w^2+h^2);       % [kg*m^2]
inertia = [Ix_sc Iy_sc Iz_sc]';
J = diag(inertia);

% - Orbital parameters taken from https://www.n2yo.com/satellite/?s=41942
r_eq = 6371.2;                       % equatorial radius of the Earth
rp = r_eq + 35788.8;                 % Perigee radius [km]
ra = r_eq + 35798.4;                 % Apogee radius  [km]
a = (ra+rp)/2;                       % Semimajor axis [km]
e = (ra-rp)/(ra+rp);                 % Eccentricity   [-]
i = deg2rad(0.0058);                 % Inclination    [rad]
RA = deg2rad(82.1426);               % Right ascension of ascending node   [rad]
periarg = deg2rad(307.9289);         % Argument of perigee                 [rad]
theta0 = deg2rad(180);               % Initial anomaly                     [rad]
muE = 398600;                        % Gravitational parameter for Earth   [km^3/s^2]
period = 2*pi*sqrt(a^3/muE);         % Orbital period [s]

% - Initial conditions - Attitude and orbit
om0_1 = deg2rad([15 11 7.8]');                              % Initial velocity  [rad/s]
[r0_SC_1, v0_SC_1] = kep2car(a,e,i,RA,periarg,theta0,muE);  % Initial r,v       [km,km/s]
X0_1 = [r0_SC_1; v0_SC_1];                                  % Initial state
q0_1 = [0.233 -0.667 0.667 0.233]';                           % Initial attitude
q0_norm_1 = q0_1 / norm(q0_1);                          

% - Coefficients for SRP disturbance
rho_s = 0.5;                         % Symmetric reflection
rho_d = 0.1;                         % Diffuse reflection
coeff = [rho_s rho_d]';
Fs = 1358;                           % Power per unit surface [W/m^2]
c = 2.998e+8;                        % Speed of light in vacuum [m/s]
P = Fs/c;                            % [Pa]

% - Areas of the faces
A_right = d*h;  A_left = A_right;    % [m^2]
A_top = w*d;    A_bott = A_top;      % [m^2]
A_front = w*h;  A_back = A_front;    % [m^2]

areas = [A_right A_top A_front A_left A_bott A_back]';

% - Characteristics of the gyro - Sensonor STIM300
%   https://www.sensonor.com/products/inertial-measurement-units/stim300/
f = 262;                                                    % [Hz]
Ts = 1/f;                                                   % Sampling time [s]                        
sigma_n = deg2rad(0.15) / sqrt(3600*Ts) * [1 1 1]';         % standard deviation  [1/s]
sigma_b = deg2rad(0.3) / (3600*sqrt(3600*Ts)) * [1 1 1]';  % std deviation [1/s^2]
mu = zeros(3,1);                                            % Mean value of the noise
seed = [67 37 41]';                                         % Characterize the random noise
om0_hat_1 = zeros(3,1)';                                    % Initial velocity for the filter model [rad/s]
b0 = zeros(3,1);                                            % Initial value for integrating the RRW
L = eye(3);                                                 % gain matrix L

% - Initialize the International Geomagnetic Reference Field (IGRF) in order
%   to compute the magnetic torque disturbance. The model and the data are
%   taken from https://www.ngdc.noaa.gov/IAGA/vmod/igrf.html. The
%   coefficients are saved in the IGRF12coeffs.xls table.

% - Initialize matrices containing g and h values
G_S = zeros(14,14);           % Schmidt normalized coefficients
H_S = zeros(13,13);

% - Number of years after the acquisition of most recent data
n_years = 3; 

% - Desired order of the polynomial expansion for the description of the
%   Earth Magnetic Field. Its values must be in the [1-13] range
grade = 13;

% - Read coefficients from IGRF12coeffs.xls table and store them into G_S,
%   H_S matrices
T = readtable('IGRF12coeffs.xls','ReadRowNames',true);

for l = 1 : length(T.Properties.RowNames)
    Row_Name = char(T.Properties.RowNames(l));
    % note that coefficient g_n^m is stored into element G_S(n+1,m+1)
    if Row_Name(1) == 'g'
        n = T{Row_Name,'n'};
        m = T{Row_Name,'m'};
        G_S(n+1,m+1) = T.x2015(Row_Name) + n_years*T.x2015_20(Row_Name);
    elseif Row_Name(1) == 'h'
        n = T{Row_Name,'n'};
        m = T{Row_Name,'m'};
        H_S(n,m) = T.x2015(Row_Name) + n_years*T.x2015_20(Row_Name);
    end
end

% - Characterization of the thrusters for de-tumbling. The chosen
%   configuration is described in "A study of spacecraft reaction thruster 
%   configurations for attitude control system", by Pasand, Hassani,
%   Ghorbani - DOI. No. 10.1109/MAES.2017.160104. In particular it's the
%   configuation number 8, as 8 thrusters are available.
%   The chosen thruster are GN_2 cold gas thrusters built by MOOG
%   aerospace, in particular the ones providing 3.6 N thrust.

u_on = deg2rad([0.3 0.3 0.3]');    % Maximum accepted velocity during detumbling  [rad/s]

B = w/2;                           % Distance along x from the center of mass  [m]
V = d/2;                           % Distance along z from the center of mass  [m]

F = 3.6;                           % Thrust [N]
t_min = 0.1;                       % Minimum response time of the thrusters  [s]

% - Matrix collecting positions of the thrusters
r_mat = [ B  B  B  B -B -B -B -B ;
          0  0  0  0  0  0  0  0 ;
         -V -V -V -V -V -V -V -V];

% - Matrix collecting forces of the thrusters
F_mat = [ 0  0 -F  0  0  0  F  0 ;
          F -F  0  0  F -F  0  0 ;
          0  0  0 -F  0  0  0 -F];

% - Initialize and compute the thrust configuration matrix
T = zeros(3,8);
for i = 1:8
    T(:,i) = cross(r_mat(:,i),F_mat(:,i));
end

% - Maximum thrust matrix, using the current thruster configuration
T_max = zeros(3,1);
T_max(1) = T(1,1) + T(1,5);
T_max(2) = T(2,3) + T(2,4);
T_max(3) = T(3,1) + T(3,6);

%% - Run simulation
t_detumble = 460;       % Simulation time for the detumbling problem  - [s]

sim('detumbling')

% Final values for the detumbling
omf_1 = rad2deg(om(end,:)');
rf_SC_1 = r_SC(end,:)';
vf_SC_1 = v_SC(end,:)';
qf_1 = q(end,:)';

%% - Plot the results

% - Uncontrolled angular velocities
% figure(1)
% plot(tout,om(:,1),tout,om(:,2),tout,om(:,3),'LineWidth',1.5)
% hold on
% line([tout(1),tout(end)],[u_on,u_on],'Color','black','LineStyle','-.',...
%     'LineWidth',1.25)
% line([tout(1),tout(end)],[-u_on,-u_on],'Color','black','LineStyle','-.',...
%     'LineWidth',1.25)
% xlabel('t [s]','Interpreter','latex','FontSize',16)
% ylabel('$\vec{\omega} \; [rad/s]$','Interpreter','latex','FontSize',16)
% legend('\omega_1','\omega_2','\omega_3')
% set(gca,'FontSize',18)
% grid minor

% - Angular velocities
figure(1)
plot(tout,om(:,1),tout,om(:,2),tout,om(:,3),'LineWidth',1.5)
hold on
line([tout(1),tout(end)],[u_on,u_on],'Color','black','LineStyle','-.',...
    'LineWidth',1.25)
line([tout(1),tout(end)],[-u_on,-u_on],'Color','black','LineStyle','-.',...
    'LineWidth',1.25)
xlabel('t [s]','Interpreter','latex','FontSize',16)
ylabel('$\vec{\omega} \; [rad/s]$','Interpreter','latex','FontSize',16)
legend('\omega_1','\omega_2','\omega_3','Velocity threshold','Interpreter','latex','FontSize',16)
set(gca,'FontSize',18)
grid minor

% - Gravity gradient disturbance
figure(2)
plot(tout,M_GG(:,1),tout,M_GG(:,2),tout,M_GG(:,3),'LineWidth',1.5)
hold on
xlabel('t [s]','Interpreter','latex','FontSize',16)
ylabel('$\vec{M}_{GG} \; [Nm]$','Interpreter','latex','FontSize',16)
legend('M_{GG,x}','M_{GG,y}','M_{GG,z}','Interpreter','latex','FontSize',16)
set(gca,'FontSize',18)
grid minor

% - Solar Radiation Pressure disturbance
figure(3)
plot(tout,M_SRP(:,1),tout,M_SRP(:,2),tout,M_SRP(:,3),'LineWidth',1.5)
hold on
xlabel('t [s]','Interpreter','latex','FontSize',16)
ylabel('$\vec{M}_{SRP} \; [Nm]$','Interpreter','latex','FontSize',16)
legend('M_{SRP,x}','M_{SRP,y}','M_{SRP,z}','Interpreter','latex','FontSize',16)
set(gca,'FontSize',18)
grid minor

% - Magnetic disturbance
figure(4)
plot(tout,M_MAG(:,1),tout,M_MAG(:,2),tout,M_MAG(:,3),'LineWidth',1.5)
hold on
xlabel('t [s]','Interpreter','latex','FontSize',16)
ylabel('$\vec{M}_{MAG} \; [Nm]$','Interpreter','latex','FontSize',16)
legend('M_{MAG,x}','M_{MAG,y}','M_{MAG,z}','Interpreter','latex','FontSize',16)
set(gca,'FontSize',18)
grid minor

% - Overall disturbance torque
figure(5)
plot(tout,M(:,1),tout,M(:,2),tout,M(:,3),'LineWidth',1.5)
hold on
xlabel('t [s]','Interpreter','latex','FontSize',16)
ylabel('$\vec{M} \; [Nm]$','Interpreter','latex','FontSize',16)
legend('M_x','M_y','M_z','Interpreter','latex','FontSize',16)
set(gca,'FontSize',18)
grid minor

% - Control torques
figure(6)
plot(t_thrust,u_thrust(:,1),t_thrust,u_thrust(:,2),t_thrust,...
    u_thrust(:,3),'LineWidth',1.15)
hold on
xlabel('t [s]','Interpreter','latex','FontSize',16)
ylabel('$\vec{u}_{thrust} \; [Nm]$','Interpreter','latex','FontSize',16)
legend('u_{thrust,x}','u_{thrust,y}','u_{thrust,z}','Interpreter','latex','FontSize',16)
set(gca,'FontSize',18)
grid minor

% - Measured angular velocity
om_m1 = squeeze(om_m(1,1,:));
om_m2 = squeeze(om_m(2,1,:));
om_m3 = squeeze(om_m(3,1,:));

figure(7)
plot(tout,om_m1,tout,om_m2,tout,om_m3,...
    'LineWidth',1.15)
hold on
xlabel('t [s]','Interpreter','latex','FontSize',16)
ylabel('$\vec{\omega}_{gyro} \; [rad/s]$','Interpreter','latex',...
    'FontSize',16)
legend('\omega_{m,x}','\omega_{m,y}','\omega_{m,z}','Interpreter','latex','FontSize',16)
set(gca,'FontSize',18)
grid minor

figure(8)
plot(tout,om_e(:,1),tout,om_e(:,2),tout,om_e(:,3),'LineWidth',1.15)
hold on
xlabel('t [s]','Interpreter','latex','FontSize',16)
ylabel('$\vec{\omega}_{est} \; [rad/s]$','Interpreter','latex',...
    'FontSize',16)
legend('\omega_{est,x}','\omega_{est,y}','\omega_{est,z}','Interpreter','latex','FontSize',16)
set(gca,'FontSize',18)
grid minor



%% - SUN POINTING PHASE - From 450 to 20450 secs

% - Starting from the final conditions of the detumbling phase, a slew and 
%   Sun pointing manoeuvre is performed

% - The initial conditions for the slew and pointing problem are the final
%  conditions obtained from the detumbling problem.
om0_2 = om(end,:)';                 % Initial angular velocity for slew & pointing
r0_SC_2 = r_SC(end,:)';             % Initial position for slew & pointing
v0_SC_2 = v_SC(end,:)';             % Initial velocity for slew & pointing
X0_2 = [r0_SC_2; v0_SC_2];
q0_2 = q(end,:)';                

% - Control parameters. The slew and pointing control is performed using a
%   3 RWs architecture which are placed along the three body axes. The
%   chosen RWs are the RSI 68-170/60 built by Rockwell-Collins, whose specs
%   can be found at http://www.electronicnote.com/RCG/HT-RSI_A4.pdf

K1 = 10; K2 = .1;

A = [1 0 0 ;
     0 1 0 ;
     0 0 1 ]; % configuration matrix for the RWs

h_r0_2 = zeros(size(A,2),1);  % Initial condition on angular momentum
h_max = 68;                   % Max angular momentum of the RW  -  [Nms]
hdot_max = 170e-3;            % Max torque provided             -  [Nm]


% - Characterization of the sensors
alpha = [1/2 1/2]';

err_SS = 1*pi/(60*180);   % 1 arc minute for the SS
err_EH = 6*pi/(60*180);   % 6 arc minute for the EH

SS_err = [cos(err_SS)*cos(err_SS)  cos(err_SS)*sin(err_SS)*sin(err_SS)+sin(err_SS)*cos(err_SS)  -cos(err_SS)*sin(err_SS)*cos(err_SS)+sin(err_SS)*sin(err_SS);
          -sin(err_SS)*cos(err_SS) -sin(err_SS)*sin(err_SS)*sin(err_SS)+cos(err_SS)*cos(err_SS) sin(err_SS)*sin(err_SS)*cos(err_SS)+cos(err_SS)*sin(err_SS) ;
          sin(err_SS)              -cos(err_SS)*sin(err_SS)                                     cos(err_SS)*cos(err_SS)                                    ];

EH_err = [cos(err_EH)*cos(err_EH)  cos(err_EH)*sin(err_EH)*sin(err_EH)+sin(err_EH)*cos(err_EH)  -cos(err_EH)*sin(err_EH)*cos(err_EH)+sin(err_EH)*sin(err_EH);
          -sin(err_EH)*cos(err_EH) -sin(err_EH)*sin(err_EH)*sin(err_EH)+cos(err_EH)*cos(err_EH) sin(err_EH)*sin(err_EH)*cos(err_EH)+cos(err_EH)*sin(err_EH) ;
          sin(err_EH)              -cos(err_EH)*sin(err_EH)                                     cos(err_EH)*cos(err_EH)                                    ];

%% RUN THE SLEW & POINTING SIMULATION
t_slew_point = 2e+4;       % Simulation time for the slew and pointing
                           %  problem  - [s]

sim('slew_pointing')

% Final values of the simulation
omf_2 = om(end,:)';
rf_SC_2 = r_SC(end,:)';
vf_SC_2 = v_SC(end,:)';
qf_2 = q_real(end,:)';

%% Plot the results

% - Plot the pointing manoeuvre

u1 = squeeze(A_BN(1,1,:));  u1_ref = squeeze(A_d(1,1,:));
u2 = squeeze(A_BN(1,2,:));  u2_ref = squeeze(A_d(2,1,:));
u3 = squeeze(A_BN(1,3,:));  u3_ref = squeeze(A_d(3,1,:));
u4 = squeeze(A_BN(2,1,:));  u4_ref = squeeze(A_d(1,2,:));
u5 = squeeze(A_BN(2,2,:));  u5_ref = squeeze(A_d(2,2,:));
u6 = squeeze(A_BN(2,3,:));  u6_ref = squeeze(A_d(3,2,:));
u7 = squeeze(A_BN(3,1,:));  u7_ref = squeeze(A_d(1,3,:));
u8 = squeeze(A_BN(3,2,:));  u8_ref = squeeze(A_d(2,3,:));
u9 = squeeze(A_BN(3,3,:));  u9_ref = squeeze(A_d(3,3,:));


% - Plot A(1,:)
figure(9)

subplot(3,1,1)
plot(tout,u1,tout,u1_ref,'LineWidth',1.5)
legend('A_{B/N}(1,1)','A_d(1,1)','Intepreter','latex')
xlabel('t [s]','Interpreter','latex')
title('$\textbf{A}_d(1,1)$ vs $\textbf{A}_{B/N}(1,1)$',...
    'Interpreter','latex')
set(gca,'FontSize',14)
grid minor

subplot(3,1,2)
plot(tout,u2,tout,u2_ref,'LineWidth',1.5)
legend('A_{B/N}(1,2)','A_d(1,2)','Interpreter','latex','FontSize',16)
xlabel('t [s]','Interpreter','latex')
title('$\textbf{A}_d(1,2)$ vs $\textbf{A}_{B/N}(1,2)$',...
    'Interpreter','latex')
set(gca,'FontSize',14)
grid minor

subplot(3,1,3)
plot(tout,u3,tout,u3_ref,'LineWidth',1.5)
legend('A_{B/N}(1,3)','A_d(1,3)','Interpreter','latex','FontSize',16)
xlabel('t [s]','Interpreter','latex')
title('$\textbf{A}_d(1,3)$ vs $\textbf{A}_{B/N}(1,3)$',...
    'Interpreter','latex')
set(gca,'FontSize',14)
grid minor

% - Plot A(2,:)
figure(10)

subplot(3,1,1)
plot(tout,u4,tout,u4_ref,'LineWidth',1.5)
legend('A_{B/N}(2,1)','A_d(2,1)','Interpreter','latex','FontSize',16)
xlabel('t [s]','Interpreter','latex')
title('$\textbf{A}_d(2,1)$ vs $\textbf{A}_{B/N}(2,1)$',...
    'Interpreter','latex')
set(gca,'FontSize',14)
grid minor

subplot(3,1,2)
plot(tout,u5,tout,u5_ref,'LineWidth',1.5)
legend('A_{B/N}(2,2)','A_d(2,2)','Interpreter','latex','FontSize',16)
xlabel('t [s]','Interpreter','latex')
title('$\textbf{A}_d(2,2)$ vs $\textbf{A}_{B/N}(2,2)$',...
    'Interpreter','latex')
set(gca,'FontSize',14)
grid minor

subplot(3,1,3)
plot(tout,u6,tout,u6_ref,'LineWidth',1.5)
legend('A_{B/N}(2,3)','A_d(2,3)','Interpreter','latex','FontSize',16)
xlabel('t [s]','Interpreter','latex')
title('$\textbf{A}_d(2,3)$ vs $\textbf{A}_{B/N}(2,3)$',...
    'Interpreter','latex')
set(gca,'FontSize',14)
grid minor

% - Plot A(3,:)
figure(11)

subplot(3,1,1)
plot(tout,u7,tout,u7_ref,'LineWidth',1.5)
legend('A_{B/N}(3,1)','A_d(3,1)','Interpreter','latex','FontSize',16)
xlabel('t [s]','Interpreter','latex')
title('$\textbf{A}_d(3,1)$ vs $\textbf{A}_{B/N}(3,1)$',...
    'Interpreter','latex')
set(gca,'FontSize',14)
grid minor

subplot(3,1,2)
plot(tout,u8,tout,u8_ref,'LineWidth',1.5)
legend('A_{B/N}(3,2)','A_d(3,2)','Interpreter','latex','FontSize',16)
xlabel('t [s]','Interpreter','latex')
title('$\textbf{A}_d(3,2)$ vs $\textbf{A}_{B/N}(3,2)$',...
    'Interpreter','latex')
set(gca,'FontSize',14)
grid minor

subplot(3,1,3)
plot(tout,u9,tout,u9_ref,'LineWidth',1.5)
legend('A_{B/N}(3,3)','A_d(3,3)','Interpreter','latex','FontSize',16)
xlabel('t [s]','Interpreter','latex')
title('$\textbf{A}_d(3,3)$ vs $\textbf{A}_{B/N}(3,3)$',...
    'Interpreter','latex')
set(gca,'FontSize',14)
grid minor

% - Plot the quaternion error
figure(12)
plot(tout,q_e(:,1),tout,q_e(:,2),tout,q_e(:,3),'LineWidth',1.5)
xlabel('t [s]','Interpreter','latex')
ylabel('$\textbf{q}_e$','Interpreter','latex')
legend('q_1','q_2','q_3','Interpreter','latex','FontSize',16)
set(gca,'FontSize',18)
grid minor

% - Plot angular velocity
figure(13)
plot(tout,om(:,1),tout,om(:,2),tout,om(:,3),'LineWidth',1.5)
hold on
xlabel('t [s]','Interpreter','latex','FontSize',16)
ylabel('$\vec{\omega} \; [rad/s]$','Interpreter','latex')
legend('\omega_{x}','\omega_{y}','\omega_{z}','Interpreter','latex','FontSize',16)
set(gca,'FontSize',18)
grid minor

% - Plot RWs torque
figure(14)
plot(tout,u(:,1),tout,u(:,2),tout,u(:,3),'LineWidth',1.5)
hold on
xlabel('t [s]','Interpreter','latex','FontSize',16)
ylabel('$\vec{u} \; [Nm]$','Interpreter','latex')
legend('u_{x}','u_{y}','u_{z}','Interpreter','latex','FontSize',16)
set(gca,'FontSize',18)
grid minor





